<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Attraction;
use Illuminate\Http\Request;
use App\Authorizable;

class AttractionsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $attractions = Attraction::where('name', 'LIKE', "%$keyword%")
                ->orWhere('thumbnail', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $attractions = Attraction::latest()->paginate($perPage);
        }

        return view('admin.attractions.index', compact('attractions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.attractions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required|unique:attractions',
			'thumbnail' => 'required',
			'image' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();

        if ($request->hasFile('thumbnail')) {
            $thumbnail=$request->file('thumbnail')->getClientOriginalName();
            $request->thumbnail->move(base_path('public/img'), $thumbnail);
            $requestData['thumbnail'] = $thumbnail;
        }
        if ($request->hasFile('image')) {
            $filename=$request->file('image')->getClientOriginalName();
            $request->image->move(base_path('public/img'), $filename);
            $requestData['image'] = $filename;
        }

        Attraction::create($requestData);

        return redirect('admin/attractions')->with('flash_message', 'Attraction added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $attraction = Attraction::findOrFail($id);

        return view('admin.attractions.show', compact('attraction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $attraction = Attraction::findOrFail($id);

        return view('admin.attractions.edit', compact('attraction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required|unique:attractions,name,'.$id,
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('thumbnail')) {
            $thumbnail=$request->file('thumbnail')->getClientOriginalName();
            $request->thumbnail->move(base_path('public/img'), $thumbnail);
            $requestData['thumbnail'] = $thumbnail;
        }
        if ($request->hasFile('image')) {
            $filename=$request->file('image')->getClientOriginalName();
            $request->image->move(base_path('public/img'), $filename);
            $requestData['image'] = $filename;
        }

        $attraction = Attraction::findOrFail($id);
        $attraction->update($requestData);

        return redirect('admin/attractions')->with('flash_message', 'Attraction updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Attraction::destroy($id);

        return redirect('admin/attractions')->with('flash_message', 'Attraction deleted!');
    }
}
