<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Bannerimage;
use Illuminate\Http\Request;
use App\Authorizable;

class BannerimagesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $bannerimages = Bannerimage::where('name', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $bannerimages = Bannerimage::latest()->paginate($perPage);
        }

        return view('admin.bannerimages.index', compact('bannerimages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.bannerimages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'image' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename=$request->file('image')->getClientOriginalName();
            $request->image->move(base_path('public/img'), $filename);
            $requestData['image'] = $filename;
        }

        Bannerimage::create($requestData);

        return redirect('admin/bannerimages')->with('flash_message', 'Bannerimage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $bannerimage = Bannerimage::findOrFail($id);

        return view('admin.bannerimages.show', compact('bannerimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $bannerimage = Bannerimage::findOrFail($id);

        return view('admin.bannerimages.edit', compact('bannerimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'image' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename=$request->file('image')->getClientOriginalName();
            $request->image->move(base_path('public/img'), $filename);
            $requestData['image'] = $filename;
        }

        $bannerimage = Bannerimage::findOrFail($id);
        $bannerimage->update($requestData);

        return redirect('admin/bannerimages')->with('flash_message', 'Bannerimage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Bannerimage::destroy($id);

        return redirect('admin/bannerimages')->with('flash_message', 'Bannerimage deleted!');
    }
}
