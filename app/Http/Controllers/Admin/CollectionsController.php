<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Collection;
use Illuminate\Http\Request;
use App\Authorizable;

class CollectionsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $collections = Collection::where('name', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $collections = Collection::latest()->paginate($perPage);
        }

        return view('admin.collections.index', compact('collections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.collections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required|unique:collections'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename=$request->file('image')->getClientOriginalName();
            $request->image->move(base_path('public/img'), $filename);
            $requestData['image'] = $filename;
        }

        Collection::create($requestData);

        return redirect('admin/collections')->with('flash_message', 'Collection added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $collection = Collection::findOrFail($id);

        return view('admin.collections.show', compact('collection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $collection = Collection::findOrFail($id);

        return view('admin.collections.edit', compact('collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required|unique:collections,name,'.$id
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
            $filename=$request->file('image')->getClientOriginalName();
            $request->image->move(base_path('public/img'), $filename);
            $requestData['image'] = $filename;
        }

        $collection = Collection::findOrFail($id);
        $collection->update($requestData);

        return redirect('admin/collections')->with('flash_message', 'Collection updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Collection::destroy($id);

        return redirect('admin/collections')->with('flash_message', 'Collection deleted!');
    }
}
