<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Homevideo;
use Illuminate\Http\Request;
use App\Authorizable;

class HomevideosController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $homevideos = Homevideo::where('name', 'LIKE', "%$keyword%")
                ->orWhere('video', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $homevideos = Homevideo::latest()->paginate($perPage);
        }

        return view('admin.homevideos.index', compact('homevideos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.homevideos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'video' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('video')) {
        $filename=$request->file('video')->getClientOriginalName();
        $request->video->move(base_path('public/video'), $filename);
        $requestData['video'] = $filename;
        }

        Homevideo::create($requestData);

        return redirect('admin/homevideos')->with('flash_message', 'Homevideo added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $homevideo = Homevideo::findOrFail($id);

        return view('admin.homevideos.show', compact('homevideo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $homevideo = Homevideo::findOrFail($id);

        return view('admin.homevideos.edit', compact('homevideo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'video' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('video')) {
            $filename=$request->file('video')->getClientOriginalName();
            $request->video->move(base_path('public/video'), $filename);
            $requestData['video'] = $filename;
        }

        $homevideo = Homevideo::findOrFail($id);
        $homevideo->update($requestData);

        return redirect('admin/homevideos')->with('flash_message', 'Homevideo updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Homevideo::destroy($id);

        return redirect('admin/homevideos')->with('flash_message', 'Homevideo deleted!');
    }
}
