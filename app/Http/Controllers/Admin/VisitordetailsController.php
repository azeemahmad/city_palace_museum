<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Visitordetail;
use Illuminate\Http\Request;
use App\Authorizable;

class VisitordetailsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $visitordetails = Visitordetail::where('name', 'LIKE', "%$keyword%")
                ->orWhere('mobile', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $visitordetails = Visitordetail::latest()->paginate($perPage);
        }

        return view('admin.visitordetails.index', compact('visitordetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.visitordetails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'mobile' => 'required',
			'date' => 'required'
		]);
        $requestData = $request->all();
        
        Visitordetail::create($requestData);

        return redirect('admin/visitordetails')->with('flash_message', 'Visitordetail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $visitordetail = Visitordetail::findOrFail($id);

        return view('admin.visitordetails.show', compact('visitordetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $visitordetail = Visitordetail::findOrFail($id);

        return view('admin.visitordetails.edit', compact('visitordetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'mobile' => 'required',
			'date' => 'required'
		]);
        $requestData = $request->all();
        
        $visitordetail = Visitordetail::findOrFail($id);
        $visitordetail->update($requestData);

        return redirect('admin/visitordetails')->with('flash_message', 'Visitordetail updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Visitordetail::destroy($id);

        return redirect('admin/visitordetails')->with('flash_message', 'Visitordetail deleted!');
    }
}
