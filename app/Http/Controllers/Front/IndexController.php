<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Homevideo;
use App\Models\About;
use App\Models\History;
use App\Models\Collection;
use App\Models\Subscription;
use Illuminate\Support\Facades\Validator;
use App\Models\Bannerimage;
use App\Models\Attraction;
use App\Models\Visitordetail;


class IndexController extends Controller
{
    public function index(){
        $homevideo=Homevideo::latest()->first();
        $aboutus=About::latest()->first();
        $history=History::latest()->first();
        $collection=Collection::whereNotNull('image')->take(3)->get();
        return view('index',compact('homevideo','aboutus','history','collection'));
    }

    public function planyourvisit()
    {
        $bannerImage=Bannerimage::latest()->first();
        $attraction=Attraction::latest()->where('status',1)->get();
        return view('users.planyourvisit',compact('bannerImage','attraction'));
    }
    public  function subscriptions(Request $request){


        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        if ($validator->fails()) {
            return 3;
        }
        $data=['email' =>$request['email'],'status' =>1];
        $emailcheck=Subscription::where('email',$request['email'])->first();
        if(!$emailcheck){
            Subscription::create($data);
            return 1;
        }
        else{
            return 2;
        }

    }

    public function clientvisitplan(Request $request){
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'name' => 'required',
            'mobile' => 'required',
        ]);
        if ($validator->fails()) {
            echo 3;
        }
        $data=[
            'date' =>date('Y-m-d',strtotime($request['date'])),
            'name' =>$request['name'],
            'mobile' => (int)$request['mobile'],
            'status' => 1
        ];
        $datasave=Visitordetail::create($data);
        if($datasave){
            echo 1;
        }
        else{
            echo 2;
        }

    }


}
