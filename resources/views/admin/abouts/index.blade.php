@extends('admin.layouts.master')
<style>
    .searchBar {
        margin-right: 22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Abouts
                        </h2>
                    </div>
                    <br>
                    <a href="{{ url('/admin/abouts/create') }}" class="btn btn-success btn-sm waves-effect"
                       title="Add New About" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                    </a>
                    {!! Form::open(['method' => 'GET', 'url' => '/admin/abouts', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search..."
                               style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                                 <i class="material-icons" style="height: 27px !important;">search</i>
                             </button>
                              </span>
                    </div>
                    {!! Form::close() !!}
                    <div class="body">
                        <br>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    @can('view_abouts','edit_abouts', 'delete_abouts')
                                    <th class="text-center">Actions</th>
                                    @endcan
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($abouts as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td><img src="{{asset('img/'.$item->image)}}" width="400px" height="350px"></td>
                                        <td width="500px">{!! $item->description !!}</td>
                                        <td>{!! $item->status==1?'<b style="color:green">Enabled</b>':'<b style="color:red">Disbaled</b>' !!}</td>
                                        @can('view_abouts')
                                        <td class="text-center">
                                            <a href="{{ url('/admin/abouts/' . $item->id) }}" title="View About">
                                                <button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i>
                                                    View
                                                </button>
                                            </a>
                                            @include('shared._actions', ['entity' => 'abouts',
                                            'id' => $item->id
                                            ])
                                        </td>
                                        @endcan
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $abouts->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
