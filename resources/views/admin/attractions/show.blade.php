@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Attraction {{ $attraction->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/attractions') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        @can('edit_attractions', 'delete_attractions')
                        <a href="{{ url('/admin/attractions/' . $attraction->id . '/edit') }}" title="Edit Attraction">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>

                        <form method="POST" action="{{ url('admin/attractions' . '/' . $attraction->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Attraction"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        @endcan
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $attraction->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $attraction->name }} </td>
                                </tr>
                                <tr>
                                    <th> Thumbnail</th>
                                    <td><img src="{{asset('img/'.$attraction->thumbnail)}}" width="250px" height="200px"></td>
                                </tr>
                                <tr>
                                    <th> Image</th>
                                    <td><img src="{{asset('img/'.$attraction->image)}}" width="250px" height="200px"></td>
                                </tr>
                                <tr>
                                    <th> Description</th>
                                    <td width="500px">{!! $attraction->description !!}</td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td>{!! $attraction->status==1?'<b style="color:green">Enabled</b>':'<b style="color:red">Disbaled</b>' !!}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
