@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            History {{ $history->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/histories') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        @can('edit_histories', 'delete_histories')
                        <a href="{{ url('/admin/histories/' . $history->id . '/edit') }}" title="Edit History">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>

                        <form method="POST" action="{{ url('admin/histories' . '/' . $history->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete History"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        @endcan
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $history->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $history->name }} </td>
                                </tr>
                                <tr>
                                    <th> Image</th>
                                    <td><img src="{{asset('img/'.$history->image)}}" width="400px" height="350px"></td>
                                </tr>
                                <tr>
                                    <th> Description</th>
                                    <td width="500px">{!! $history->description !!}</td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td>{!! $history->status==1?'<b style="color:green">Enabled</b>':'<b style="color:red">Disbaled</b>' !!}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
