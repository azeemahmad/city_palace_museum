<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            @if(Auth::guard('admin')->user()->image)
                <img src="{{asset('admin/images/profile_image/'.Auth::guard('admin')->user()->image)}}" width="48" height="48" alt="User" />
                @else
            <img src="{{asset('admin/images/user.png')}}" width="48" height="48" alt="User" />
                @endif
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::guard('admin')->user()->name}}</div>
            <div class="email">{{Auth::guard('admin')->user()->email}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">

                    <li><a href="{{url('/admin/profile')}}"><i class="material-icons">person</i>Profile</a></li>
                    <li role="seperator" class="divider"></li>
                    <li><a href="{{url('/admin/logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
                    <li role="seperator" class="divider"></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{url('admin/home')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
            @can('view_users')
            <li><a href="{{'/admin/users'}}"><i class="material-icons">person_add</i><span>Users</span></a>
            @endcan
             @can('view_roles')
            <li><a href="{{'/admin/roles'}}"><i class="material-icons">book</i><span>Role</span></a></li>
            @endcan

            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">folder_open</i>
                    <span>Home Page</span>
                </a>
                <ul class="ml-menu">
                    @can('view_homevideos')
                    <li>
                        <a href="{{url('admin/homevideos')}}"> <i class="material-icons">videocam</i> <span> Banner Video</span></a>
                    </li>
                    @endcan
                    @can('view_abouts')
                    <li>
                        <a href="{{url('admin/abouts')}}"> <i class="material-icons">perm_device_information</i><span> About us</span></a>
                    </li>
                    @endcan
                    @can('view_histories')
                    <li>
                        <a href="{{url('admin/histories')}}"> <i class="material-icons">history</i><span> History</span></a>
                    </li>
                    @endcan
                    @can('view_collections')
                    <li>
                        <a href="{{url('admin/collections')}}"> <i class="material-icons">folder</i><span> Galleries</span></a>
                    </li>
                    @endcan
                    @can('view_subscriptions')
                    <li>
                        <a href="{{url('admin/subscriptions')}}"> <i class="material-icons">person_pin</i><span> User Subscriptions</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
            <li>
                <a href="javascript:void(0);" class="menu-toggle">
                    <i class="material-icons">flight_takeoff</i>
                    <span>Plan Visit</span>
                </a>
                <ul class="ml-menu">
                    @can('view_bannerimages')
                    <li>
                        <a href="{{url('admin/bannerimages')}}"><i class="material-icons">image</i><span> Banner Image</span></a>
                    </li>
                    @endcan
                    @can('view_attractions')
                    <li>
                        <a href="{{url('admin/attractions')}}"><i class="material-icons">place</i><span> Attractions</span></a>
                    </li>
                    @endcan
                    @can('view_visitordetails')
                    <li>
                        <a href="{{url('admin/visitordetails')}}"><i class="material-icons">transfer_within_a_station</i><span> Visitor Plan</span></a>
                    </li>
                    @endcan
                </ul>
            </li>

        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; {{date('Y')}} <a href="{{url('/')}}" target="_blank">City Palace Museum</a>.
        </div>
    </div>
    <!-- #Footer -->
</aside>