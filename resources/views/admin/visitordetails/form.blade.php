 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($visitordetail->name) ? $visitordetail->name : ''}}" required>
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="mobile" style="font-size:18px;">{{ 'Mobile' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="mobile" type="number" id="mobile" value="{{ isset($visitordetail->mobile) ? $visitordetail->mobile : ''}}" >
     {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="date" style="font-size:18px;">{{ 'Date' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="date" type="date" id="date" value="{{ isset($visitordetail->date) ? $visitordetail->date : ''}}" required>
     {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($visitordetail->status) && $visitordetail->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
