@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="card">
                    <div class="header" style="background: #e2d1d1">
                        <h2>
                            Visitordetail {{ $visitordetail->id }}
                        </h2>
                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/visitordetails') }}" title="Back">
                            <button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back
                            </button>
                        </a>
                        @can('edit_visitordetails', 'delete_visitordetails')
                        <a href="{{ url('/admin/visitordetails/' . $visitordetail->id . '/edit') }}"
                           title="Edit Visitordetail">
                            <button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button>
                        </a>

                        <form method="POST" action="{{ url('admin/visitordetails' . '/' . $visitordetail->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Visitordetail"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i>
                                Delete
                            </button>
                        </form>
                        @endcan
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $visitordetail->id }}</td>
                                </tr>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $visitordetail->name }} </td>
                                </tr>
                                <tr>
                                    <th> Mobile</th>
                                    <td> {{ $visitordetail->mobile }} </td>
                                </tr>
                                <tr>
                                    <th> Visit Dat</th>
                                    <td>{{ date('d F Y',strtotime($visitordetail->date)) }}</td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td>{!! $visitordetail->status==1?'<b style="color:green">Enabled</b>':'<b style="color:red">Disbaled</b>' !!}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
