<style>
    #footer .footer-top .footer-newsletter input[type="button"] {
        background: #000000;
        border: 1px solid #000000;
        width: 35%;
        padding: 3px 0px 8px;
        text-align: center;
        color: #fff;
        text-transform: uppercase;
        font-size: 20px;
        transition: 0.3s;
        cursor: pointer;
        border-radius: 0px 20px 20px 0px;
    }
</style>
<footer id="footer" class="section-bg">
    <div class="footer-top">
        <div class="container-fluid">

            <div class="row">

                <div class="col-md-10 offset-lg-1">

                    <div class="col-lg-7 float-left fleft-block">

                        <div class="row">
                            <div class="col-lg-12 footer-newsletter">
                                <h4 class="footer-h4">Subscribe to RSS Feed: </h4>
                                <strong id="displayMessage"></strong>
                                <form id="subscriptionsform">
                                    <input type="email" name="email" id="useremail"><input type="submit"  value="Subscribe" id="submitsubscriptionform">
                                </form>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-sm-7">

                                <div class="footer-info">
                                    <h4 class="footer-h4">City Palace Museum Udaipur India </h4>
                                </div>

                                <div class="footer-info">
                                    <h6>&copy 2019 Eternal Mewar. All rights reserved. </h6>
                                </div>

                            </div>

                            <div class="col-sm-5">
                                <div class="social-links footer-info">
                                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                    <a href="#" class="youtube"><i class="fa fa-youtube"></i></a>
                                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                                </div>
                                <div class="footer-info">
                                    <h6>Powered by <a href="https://www.firsteconomy.com/" target="_blank">First Economy</a> </h6>
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-lg-5 float-left fright-block">

                        <div class="map-block">
                            <iframe src="https://www.google.com/maps/d/embed?mid=10kQYC16HWQUBM9muakeNLz3lhNgZJoM0" width="100%" height="400"></iframe>
                        </div>

                    </div>

                </div>



            </div>

        </div>
    </div>

    <div class="container">
        <div class="copyright">
            <a href="#" target="_blank">Acknowledgements</a> | <a href="#" target="_blank">Newsletter</a> | <a href="#" target="_blank">Accessibility Statement</a> | <a href="#" target="_blank">Site Map</a> | <a href="#" target="_blank">Terms & Conditions</a> | <a href="#" target="_blank">Privacy Statement</a> | <a href="#" target="_blank">Redressal Forum</a> | <a href="#" target="_blank">PAMM - Access Policy & Guidelines</a>
            <p>Copyright &copy 2019. Shriji Arvind Singh Mewar of Udaipur-Concept Developer, Eternal Mewar, All Rights Reserved </p>
        </div>
        <div class="credits">
        </div>
    </div>
</footer><!-- #footer -->

