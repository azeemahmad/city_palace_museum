<header id="header">
    <div class="container-fluid">
        <div class="logo float-left col-lg-2 offset-lg-2">
            <a href="http://dev.firsteconomy.com/city-palace-museum/" class="scrollto"><img src="{{asset('img/logo.png')}}" alt="" class="img-fluid"></a>
        </div>
        <nav class="main-nav float-right d-none d-lg-block col-lg-8">
            <ul>
                <li class="active"><a href="{{url('/')}}">Home</a></li>
                <li><a href="#">ABOUT US</a></li>
                <li><a href="#">RESEARCH &<br> CONSERVATION</a></li>
                <li><a href="#">COLLECTIONS</a></li>
                <li><a href="#">GALLERY</a></li>
                <li><a href="#">JOIN &<br>SUPPORT</a></li>
                <li><a href="{{url('/planyourvisit')}}">PLAN YOUR VISIT</a></li>
                <li><a href="#">CONTACT US</a></li>
            </ul>
        </nav>
    </div>
</header>