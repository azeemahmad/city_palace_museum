<script type="text/javascript">
    $(function() {
        $('a[href*=#]').on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 500, 'linear');
        });
    });
    $('#submitsubscriptionform').click(function(event){
        event.preventDefault();
        var email = $('#useremail').val();
        if(email==''){
            alert('Enter valid email !');
            return false;
        }
        else{
            $.ajax({
                url: "/subscriptions",
                type: 'GEt',
                data: {email:email},
                success: function (response) {
                    console.log(response);
                    if(response==1){
                        $('#displayMessage').html('Your Subscription done successfully!');
                        $('#displayMessage').css('color','green');
                        $("#subscriptionsform").trigger("reset");
                    }
                    else if(response==2){
                        $('#displayMessage').html('You have already subscribed!');
                        $('#displayMessage').css('color','green');
                    }
                    else{
                        $('#displayMessage').html('Please enter valid email');
                        $('#displayMessage').css('color','red');
                        $("#subscriptionsform").trigger("reset");
                    }

                }
            });
        }
    });

    $('#visitorsubmitbutton').click(function(event){
        event.preventDefault();
        var date = $('#visitordate').val();
        if(date==''){
            alert('Enter valid date !');
            return false;
        }
        var name = $('#visitorname').val();
        if(name==''){
            alert('Enter valid name !');
            return false;
        }
        var mobile = $('#visitormobile').val();
        if(mobile==''){
            alert('Enter valid mobile !');
            return false;
        }
        if(mobile.length != 10){
            alert('Enter 10 digit mobile number !');
            return false;
        }
        $.ajax({
            type:"POST",
            url:"/clientvisitplan",
            data:{date:date,name:name,mobile:mobile},
            success: function(response){
                console.log(response);
                if(response==1){
                    $('#dispalysuccesssmessage').html('Your visiting plan submitted successfully !');
                    $('#dispalysuccesssmessage').css('color','green');
                    $("#msform").trigger("reset");
                }
                else{
                    $('#dispalysuccesssmessage').html('Something went wrong ! please try again.');
                    $('#dispalysuccesssmessage').css('color','red');
                    $("#msform").trigger("reset");
                }
            }
        });
    });
</script>