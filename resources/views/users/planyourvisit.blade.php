@extends('users.layouts.master')
@section('content')
    <section id="intro" class="clearfix">
        @if($bannerImage)
            <img src="{{'img/'.$bannerImage->image}}" class="planbanner">
            @else
            <img src="img/banner.jpg" class="planbanner">
            @endif
        <section id="scrolldown" class="scrolldown">
            <!-- <h1>Scroll Down Button #1</h1> -->
            <a href="#why-us">
                <!-- <img src="http://dev.firsteconomy.com/city-palace-museum/img/logo.png"><br> -->

        <span class="wow fadeInDown"><i class="fa fa-angle-down" aria-hidden="true"></i>
</span><br>Scroll Down</a>
        </section>
    </section><!-- #intro -->


    <!--==========================
        Why Us Section starts
      ============================-->
    <section id="why-us" class="wow fadeIn">
        <div class="container-fluid no-padding">

            <!-- <header class="section-header">
              <h3>Why choose us?</h3>
              <p>Laudem latine persequeris id sed, ex fabulas delectus quo. No vel partiendo abhorreant vituperatoribus.</p>
            </header> -->

            <div class="row about">

                <div class="col-lg-3 offset-lg-1 no-padding wow fadeInLeft" data-wow-duration="1.4s">
                    <div class="calendar-outer">
                        <!-- multistep form -->

                        <form id="msform">

                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active firstchild"><!-- Account Setup --></li>
                                <li><!-- Social Profiles --></li>
                                <li><!-- Personal Details --></li>
                            </ul>
                            <strong id="dispalysuccesssmessage"></strong>
                            <!-- fieldsets -->
                            <fieldset>
                                <h2 class="fs-title">WHAT DAY ARE YOU PLANNING TO EXPLORE THE MUSEUM</h2>

                                <div class="divider"></div>
                                <h3 class="fs-subtitle">Just skip to the next step if you don't know.</h3>

                                <div id="datepicker" class="input-group date" data-date-format="dd-mm-yyyy">
                                    <input class="form-control" type="text" name="date" id="visitordate" readonly/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                <input type="button" name="next" class="next action-button" value="Next Step"/>
                            </fieldset>
                            <fieldset>
                                <!-- <h2 class="fs-title">Social Profiles</h2>
                                <h3 class="fs-subtitle">Your presence on the social network</h3>
                                <input type="text" name="twitter" placeholder="Twitter" />
                                <input type="text" name="facebook" placeholder="Facebook" />
                                <input type="text" name="gplus" placeholder="Google Plus" /> -->
                                <h2 class="fs-title">WHAT DAY ARE YOU PLANNING TO EXPLORE THE MUSEUM</h2>

                                <div class="divider"></div>
                                <h3 class="fs-subtitle">Just skip to the next step if you don't know.</h3>

                                <input class="form-control text-input" type="text" name="name" id="visitorname" placeholder="Enter Name"/>

                                <input type="button" name="previous" class="previous action-button" value="Previous"/>
                                <input type="button" name="next" class="next action-button" value="Next"/>
                            </fieldset>
                            <fieldset>
                                <!-- <h2 class="fs-title">Personal Details</h2>
                                <h3 class="fs-subtitle">We will never sell it</h3>
                                <input type="text" name="fname" placeholder="First Name" />
                                <input type="text" name="lname" placeholder="Last Name" />
                                <input type="text" name="phone" placeholder="Phone" />
                                <textarea name="address" placeholder="Address"></textarea> -->
                                <h2 class="fs-title">WHAT DAY ARE YOU PLANNING TO EXPLORE THE MUSEUM</h2>

                                <div class="divider"></div>
                                <h3 class="fs-subtitle">Just skip to the next step if you don't know.</h3>
                                <input class="form-control text-input" type="number" name="mobile" id="visitormobile" placeholder="Enter Phone no."/>
                                <input type="button" name="previous" class="previous action-button" value="Previous"/>
                                <input type="submit" name="submit" id="visitorsubmitbutton" class="submit action-button" value="Submit"/>
                            </fieldset>
                        </form>
                    </div>
                </div>

                <div class="col-lg-1"></div>

                <div class="col-lg-7 no-padding wow fadeInRight" data-wow-duration="1.4s">
                    <div class="why-us-content">

                        <div class="row">
                            <!-- <div class="col-lg-4 icon-outer">
                              <img src="img/icon.png" class="icon">
                              <h4>ABOUT US:</h4>
                              <a class="cta-btn align-middle" href="#" target="_blank">Read More</a>
                              <div class="icon-divider"></div>
                            </div> -->

                            <div class="why-us-content-inner time-text col-lg-8">

                                <div class="col-lg-2">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                </div>

                                <div class="col-lg-8">
                                    <h4>VISITING HOURS</h4>

                                    <h3>09:30 AM - 05:30 PM</h3>
                                </div>

                            </div>
                        </div>

                    </div>


                </div>

            </div>

            <div class="row attractions-slider">

                <div class="container-fluid no-rpadding">
                    <!-- <div class="row"> -->
                    <div class="col-lg-10 offset-lg-1 no-rpadding">

                        <div id="carousel" class="flexslider col-lg-8 offset-lg-4">
                            <h2>ATTRACTIONS:</h2>
                            <ul class="slides ">
                                @if(isset($attraction) && $attraction->isNotEmpty())
                                @foreach($attraction as $attr)
                                        <li class="thumb">
                                            <img src="{{'img/'.$attr->thumbnail}}"/>
                                            <h4>{{ucwords($attr->name)}}</h4>
                                        </li>
                                @endforeach
                                    @else
                                    <li class="thumb">
                                        <img src="img/thumb1.jpg"/>
                                        <h4>Manek Chowk</h4>
                                    </li>
                                    <li class="thumb">
                                        <img src="img/thumb2.jpg"/>
                                        <h4>Mor Chowk</h4>
                                    </li>
                                    <li class="thumb">
                                        <img src="img/thumb3.jpg"/>
                                        <h4>Baadi Mahal</h4>
                                    </li>
                                    <li class="thumb">
                                        <img src="img/thumb4.jpg"/>
                                        <h4>Moti Mahal</h4>
                                    </li>
                                    <li class="thumb">
                                        <img src="img/thumb1.jpg"/>
                                        <h4>Manek Chowk</h4>
                                    </li>
                                    <li class="thumb">
                                        <img src="img/thumb2.jpg"/>
                                        <h4>Mor Chowk</h4>
                                    </li>
                                    <li class="thumb">
                                        <img src="img/thumb3.jpg"/>
                                        <h4>Baadi Mahal</h4>
                                    </li>
                                    <li class="thumb">
                                        <img src="img/thumb4.jpg"/>
                                        <h4>Moti Mahal</h4>
                                    </li>
                                @endif
                            </ul>
                        </div>

                        <div id="slider" class="flexslider col-lg-12">
                            <ul class="slides">
                                @if(isset($attraction) && $attraction->isNotEmpty())
                                    @foreach($attraction as $attr)
                                        <li>
                                            <img src="{{'img/'.$attr->image}}"/>

                                            <div class="slider-text">
                                                <h3>{{ucwords($attr->name)}}:</h3>

                                                <div class="icon-divider"></div>
                                                  {!! $attr->description !!}
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    <li>
                                        <img src="img/bigthumb1.jpg"/>

                                        <div class="slider-text">
                                            <h3>Manek Chowk:</h3>

                                            <div class="icon-divider"></div>
                                            <p>Manek Chowk is a huge arena opposite the palace facade and
                                                center for several activities. The part of Manek Chowk opposite
                                                the ‘Toran Pol’ is said to be the venue where the elephant fights
                                                would be organized by the Kings which was a way to test their
                                                prowess prior to the wars.</p>

                                            <p>
                                                Surya Gokhada is an open gallery in the imposing facade of the
                                                palace with carved balustrades. It is said that the king would
                                                address the public from the very spot. The ground floor and the
                                                building opposite to the palace houses several handicrafts.
                                                textile and souvenir shops which were once used as horse stables.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="img/bigthumb2.jpg"/>

                                        <div class="slider-text">
                                            <h3>Manek Chowk:</h3>

                                            <div class="icon-divider"></div>
                                            <p>Manek Chowk is a huge arena opposite the palace facade and
                                                center for several activities. The part of Manek Chowk opposite
                                                the ‘Toran Pol’ is said to be the venue where the elephant fights
                                                would be organized by the Kings which was a way to test their
                                                prowess prior to the wars.</p>

                                            <p>
                                                Surya Gokhada is an open gallery in the imposing facade of the
                                                palace with carved balustrades. It is said that the king would
                                                address the public from the very spot. The ground floor and the
                                                building opposite to the palace houses several handicrafts.
                                                textile and souvenir shops which were once used as horse stables.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="img/bigthumb1.jpg"/>

                                        <div class="slider-text">
                                            <h3>Manek Chowk:</h3>

                                            <div class="icon-divider"></div>
                                            <p>Manek Chowk is a huge arena opposite the palace facade and
                                                center for several activities. The part of Manek Chowk opposite
                                                the ‘Toran Pol’ is said to be the venue where the elephant fights
                                                would be organized by the Kings which was a way to test their
                                                prowess prior to the wars.</p>

                                            <p>
                                                Surya Gokhada is an open gallery in the imposing facade of the
                                                palace with carved balustrades. It is said that the king would
                                                address the public from the very spot. The ground floor and the
                                                building opposite to the palace houses several handicrafts.
                                                textile and souvenir shops which were once used as horse stables.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="img/bigthumb2.jpg"/>

                                        <div class="slider-text">
                                            <h3>Manek Chowk:</h3>

                                            <div class="icon-divider"></div>
                                            <p>Manek Chowk is a huge arena opposite the palace facade and
                                                center for several activities. The part of Manek Chowk opposite
                                                the ‘Toran Pol’ is said to be the venue where the elephant fights
                                                would be organized by the Kings which was a way to test their
                                                prowess prior to the wars.</p>

                                            <p>
                                                Surya Gokhada is an open gallery in the imposing facade of the
                                                palace with carved balustrades. It is said that the king would
                                                address the public from the very spot. The ground floor and the
                                                building opposite to the palace houses several handicrafts.
                                                textile and souvenir shops which were once used as horse stables.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="img/bigthumb1.jpg"/>

                                        <div class="slider-text">
                                            <h3>Manek Chowk:</h3>

                                            <div class="icon-divider"></div>
                                            <p>Manek Chowk is a huge arena opposite the palace facade and
                                                center for several activities. The part of Manek Chowk opposite
                                                the ‘Toran Pol’ is said to be the venue where the elephant fights
                                                would be organized by the Kings which was a way to test their
                                                prowess prior to the wars.</p>

                                            <p>
                                                Surya Gokhada is an open gallery in the imposing facade of the
                                                palace with carved balustrades. It is said that the king would
                                                address the public from the very spot. The ground floor and the
                                                building opposite to the palace houses several handicrafts.
                                                textile and souvenir shops which were once used as horse stables.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="img/bigthumb2.jpg"/>

                                        <div class="slider-text">
                                            <h3>Manek Chowk:</h3>

                                            <div class="icon-divider"></div>
                                            <p>Manek Chowk is a huge arena opposite the palace facade and
                                                center for several activities. The part of Manek Chowk opposite
                                                the ‘Toran Pol’ is said to be the venue where the elephant fights
                                                would be organized by the Kings which was a way to test their
                                                prowess prior to the wars.</p>

                                            <p>
                                                Surya Gokhada is an open gallery in the imposing facade of the
                                                palace with carved balustrades. It is said that the king would
                                                address the public from the very spot. The ground floor and the
                                                building opposite to the palace houses several handicrafts.
                                                textile and souvenir shops which were once used as horse stables.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="img/bigthumb1.jpg"/>

                                        <div class="slider-text">
                                            <h3>Manek Chowk:</h3>

                                            <div class="icon-divider"></div>
                                            <p>Manek Chowk is a huge arena opposite the palace facade and
                                                center for several activities. The part of Manek Chowk opposite
                                                the ‘Toran Pol’ is said to be the venue where the elephant fights
                                                would be organized by the Kings which was a way to test their
                                                prowess prior to the wars.</p>

                                            <p>
                                                Surya Gokhada is an open gallery in the imposing facade of the
                                                palace with carved balustrades. It is said that the king would
                                                address the public from the very spot. The ground floor and the
                                                building opposite to the palace houses several handicrafts.
                                                textile and souvenir shops which were once used as horse stables.</p>
                                        </div>
                                    </li>
                                    <li>
                                        <img src="img/bigthumb2.jpg"/>

                                        <div class="slider-text">
                                            <h3>Manek Chowk:</h3>

                                            <div class="icon-divider"></div>
                                            <p>Manek Chowk is a huge arena opposite the palace facade and
                                                center for several activities. The part of Manek Chowk opposite
                                                the ‘Toran Pol’ is said to be the venue where the elephant fights
                                                would be organized by the Kings which was a way to test their
                                                prowess prior to the wars.</p>

                                            <p>
                                                Surya Gokhada is an open gallery in the imposing facade of the
                                                palace with carved balustrades. It is said that the king would
                                                address the public from the very spot. The ground floor and the
                                                building opposite to the palace houses several handicrafts.
                                                textile and souvenir shops which were once used as horse stables.</p>
                                        </div>
                                    </li>
                                @endif

                            </ul>
                        </div>

                    </div>
                    <!-- </div> -->
                </div>
                <!--/ container end -->

            </div>


        </div>
    </section>

    <!--==========================
      Why Us Section ends
    ============================-->


    <!--============================
      how to reach section
    ===========================-->
    <section class="how-to-reach-section">
        <div class="container-fluid">
            <div class="row">
                <h3 class="title how-to-reach  col-lg-3 offset-lg-2">
                    HOW TO REACH
                </h3>
            </div>

            <div class="row ">

                <div class="col-lg-5 offset-lg-2 no-padding wow fadeInLeft" data-wow-duration="1.4s">

                    <div class="how-to-reach-outer">

                        <div class="row">
                            <h4><strong>CITY PALACE IN UDAIPUR</strong></h4>

                            <p>City Palace is a jewel on the banks of Lake Pichola and houses several royal palaces
                                within its premises. This is why City palace of Udaipur is known as “A palace of
                                palaces” and boasts of an ornamental style that blends European, Medieval and Chinese
                                Architecture. You can view a variety of architectural features inside these palaces
                                including attractive mirror work, beautiful paintings. traditional royal furniture and
                                ornamental tiles work</p>


                            <div class="tabs" id="myTabs">
                                <ul class="tabs-nav">
                                    <li><a href="#tab-1">BY AIR</a></li>
                                    <li><a href="#tab-2">BY RAIL</a></li>
                                    <li><a href="#tab-3">BY ROAD</a></li>
                                </ul>
                                <div class="tabs-stage">
                                    <div id="tab-1">
                                        <p>If travelling by air. board a flight for Maharana Pratap Airport in Udaipur.
                                            The airport is at a distance of 20 km from the centre of the city and has
                                            good connectivity with major Indian cities like Delhi. Mumbai, Jaipur and
                                            Ahmedabad</p>

                                        <div class="mytabbtn">
                                            <a href="#">AIR <i class="fa fa-plane" aria-hidden="true"></i></a>
                                        </div>
                                        <h4>NEW DELHI TO UDAIPUR</h4>

                                        <p>You can travel from New Delhi to Udaipur by booking a flight. The
                                            total flight time between New Delhi & Udaipur is around 1 hour 15
                                            minutes.</p>
                                    </div>
                                    <div id="tab-2">
                                        <p>lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit
                                            amet, lorem ipsum dolor sit amet.</p>

                                        <div class="mytabbtn">
                                            <a href="#">RAIL <i class="fa fa-train" aria-hidden="true"></i></a>
                                        </div>
                                        <h4>NEW DELHI TO UDAIPUR</h4>

                                        <p>lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit
                                            amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet.</p>
                                    </div>
                                    <div id="tab-3">
                                        <p>lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit
                                            amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit amet.</p>

                                        <div class="mytabbtn">
                                            <a href="#">ROAD <i class="fa fa-road" aria-hidden="true"></i></a>
                                        </div>
                                        <h4>NEW DELHI TO UDAIPUR</h4>

                                        <p>lorem ipsum dolor sit amet, lorem ipsum dolor sit amet, lorem ipsum dolor sit
                                            amet, lorem ipsum dolor sit amet.</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-lg-4 no-padding wow fadeInRight reachimg" data-wow-duration="1.4s">
                    <div class="why-us-img">
                        <img src="img/bigthumb2.jpg" alt="" class="img-fluid">
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
